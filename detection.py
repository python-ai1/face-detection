import cv2
import numpy as np

face_detect = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
cam = cv2.VideoCapture(0)
# address = "https://192.168.100.2:8080"
# cam.open(address)

while True:
    ret, image = cam.read()
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    faces = face_detect.detectMultiScale(gray)
    for(x,y,w,h) in faces:
        cv2.rectangle(image,(x,y), (x+w,y+h), (0,255,0), 2)
    cv2.imshow("gottcha",image)
    if(cv2.waitKey(1) == ord('q')):
        break
cam.release()
cv2.destroyAllWindows